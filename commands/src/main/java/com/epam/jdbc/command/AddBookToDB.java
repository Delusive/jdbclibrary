package com.epam.jdbc.command;

import com.epam.jdbc.dao.IBookDao;
import com.epam.jdbc.entity.Book;
import com.epam.jdbc.entity.User;
import com.epam.jdbc.mics.IDisplayManager;

import java.sql.Date;
import java.util.Properties;
import java.util.Scanner;

public class AddBookToDB implements IExecutableCommand {
    private final IBookDao BOOK_DAO;
    private final IDisplayManager DISPLAY_MANAGER;
    private final Properties PROPS;

    public AddBookToDB(IBookDao bookDao, IDisplayManager displayManager, Properties localization) {
        this.BOOK_DAO = bookDao;
        this.DISPLAY_MANAGER = displayManager;
        PROPS = localization;
    }

    @Override
    public void execute() {
        execute(BOOK_DAO, DISPLAY_MANAGER);
    }

    @Override
    public String getDescription() {
        return PROPS.getProperty("command.addbook.description");
    }

    @Override
    public User.Role getAllowedForRole() {
        return User.Role.ADMIN;
    }

    private void execute(IBookDao bookDao, IDisplayManager displayManager) {
        Scanner in = new Scanner(System.in);
        displayManager.displayMessage(PROPS.getProperty("command.addbook.enterisbn"));
        long isbn;
        while (true) {
            try {
                isbn = Long.parseLong(in.nextLine());
                break;
            } catch (NumberFormatException e) {
                displayManager.displayMessage(PROPS.getProperty("command.addbook.incorrectisbn"));
            }
        }
        displayManager.displayMessage(PROPS.getProperty("command.addbook.enterbookname"));
        String bookName = in.nextLine();
        displayManager.displayMessage(PROPS.getProperty("command.addbook.enterauthorname"));
        String authorName = in.nextLine();
        displayManager.displayMessage(PROPS.getProperty("command.addbook.enterdate"));
        Date publishDate;
        while (true) {
            try {
                publishDate = Date.valueOf(in.nextLine());
                break;
            } catch (IllegalArgumentException e) {
                displayManager.displayMessage(PROPS.getProperty("command.addbook.invaliddate"));
            }
        }
        Book book = new Book(isbn, bookName, authorName, publishDate);
        if (bookDao.addBookToDatabase(book)) {
            displayManager.displayMessage(PROPS.getProperty("command.addbook.success"));
        } else {
            displayManager.displayMessage(PROPS.getProperty("command.addbook.error"));
        }

    }

}
