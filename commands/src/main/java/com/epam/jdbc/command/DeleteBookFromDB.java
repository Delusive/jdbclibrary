package com.epam.jdbc.command;

import com.epam.jdbc.dao.IBookDao;
import com.epam.jdbc.entity.Book;
import com.epam.jdbc.entity.User;
import com.epam.jdbc.mics.IDisplayManager;

import java.util.Properties;
import java.util.Scanner;

public class DeleteBookFromDB implements IExecutableCommand {
    private final IDisplayManager DISPLAY_MANAGER;
    private final IBookDao BOOK_DAO;
    private final Properties PROPS;

    public DeleteBookFromDB(IDisplayManager displayManager, IBookDao bookDao, Properties props) {
        DISPLAY_MANAGER = displayManager;
        BOOK_DAO = bookDao;
        PROPS = props;
    }

    @Override
    public void execute() {
        execute(BOOK_DAO, DISPLAY_MANAGER);
    }

    private void execute(IBookDao bookDao, IDisplayManager displayManager) {
        Scanner in = new Scanner(System.in);
        displayManager.displayMessage(PROPS.getProperty("command.deletebook.welcome"));
        while (true) {
            try {
                long isbn = Long.parseLong(in.nextLine());
                Book book = bookDao.getBookByISBN(isbn);
                if (book == null) {
                    displayManager.displayMessage(PROPS.getProperty("command.deletebook.notfound"));
                    if (CommandUtils.yesOrNoDialog(in, displayManager)) {
                        execute();
                    }
                    return;
                }
                displayManager.displayMessage(PROPS.getProperty("command.deletebook.isthat"));
                displayManager.displayMessage(book.toString());

                if (CommandUtils.yesOrNoDialog(in, displayManager)) {
                    if (bookDao.deleteBookByIsbn(isbn)) {
                        displayManager.displayMessage(PROPS.getProperty("command.deletebook.success"));
                    } else {
                        displayManager.displayMessage(PROPS.getProperty("command.deletebook.error"));
                    }
                    return;
                } else {
                    displayManager.displayMessage(PROPS.getProperty("command.deletebook.wrongbook"));
                }
            } catch (NumberFormatException e) {
                displayManager.displayMessage(PROPS.getProperty("command.deletebook.invalidisbn"));
            }
        }
    }

    @Override
    public String getDescription() {
        return PROPS.getProperty("command.deletebook.description");
    }

    @Override
    public User.Role getAllowedForRole() {
        return User.Role.ADMIN;
    }
}
