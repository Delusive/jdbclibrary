package com.epam.jdbc.command;

import com.epam.jdbc.entity.User;

import java.util.*;

public class CommandManager {
    private static List<IExecutableCommand> registeredCommands = new ArrayList<>();

    public static void registerCommand(IExecutableCommand command) {
        registeredCommands.add(command);
    }

    public static List<IExecutableCommand> getAllowedCommandsFor(User.Role role) {
        List<IExecutableCommand> result = new ArrayList<>();
        registeredCommands.forEach(command -> {
            if (role.hasParentOrEquals(command.getAllowedForRole())) result.add(command);
        });
        return result;
    }

}
