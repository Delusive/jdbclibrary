package com.epam.jdbc.command;

import com.epam.jdbc.dao.IAuthorDao;
import com.epam.jdbc.dao.IBookDao;
import com.epam.jdbc.dao.IBookmarkDao;
import com.epam.jdbc.entity.Author;
import com.epam.jdbc.entity.Book;
import com.epam.jdbc.entity.User;
import com.epam.jdbc.mics.IDisplayManager;

import java.util.*;

public class SearchBooksAndMakingBookmarks implements IExecutableCommand {
    private final IBookDao BOOK_DAO;
    private final IAuthorDao AUTHOR_DAO;
    private final IDisplayManager DISPLAY_MANAGER;
    private final IBookmarkDao BOOKMARK_DAO;
    private final User USER;
    private final Properties PROPS;

    public SearchBooksAndMakingBookmarks(IBookDao bookDao, IAuthorDao authorDao, IBookmarkDao bookmarkDao, IDisplayManager displayManager, User user, Properties props) {
        BOOK_DAO = bookDao;
        AUTHOR_DAO = authorDao;
        DISPLAY_MANAGER = displayManager;
        BOOKMARK_DAO = bookmarkDao;
        USER = user;
        PROPS = props;
    }

    @Override
    public void execute() {
        execute(BOOK_DAO, AUTHOR_DAO, DISPLAY_MANAGER);
    }

    @Override
    public String getDescription() {
        return PROPS.getProperty("command.searchbooks.description");
    }

    @Override
    public User.Role getAllowedForRole() {
        return User.Role.BASE;
    }

    private void execute(IBookDao bookDao, IAuthorDao authorDao, IDisplayManager displayManager) {
        Scanner in = new Scanner(System.in);
        displayManager.displayMessage(PROPS.getProperty("command.searchbooks.welcome"));
        displayManager.displayMessage(PROPS.getProperty("command.searchbooks.enterisbn"));
        String scannedLine = in.nextLine();
        //Если isbn указан, то поиск будет идти только по нему:
        while (!scannedLine.equals("")) {
            try {
                long isbn = Long.parseLong(scannedLine);
                Book book = bookDao.getBookByISBN(isbn);
                if (book == null) {
                    displayManager.displayMessage(PROPS.getProperty("command.searchbooks.booknotfoundbyisbn"));
                    if (CommandUtils.yesOrNoDialog(in, displayManager)) {
                        execute();
                    }
                    return;
                }
                displayBooksAndOfferToDoBookmark(Collections.singletonList(book));
                return;
            } catch (NumberFormatException e) {
                displayManager.displayMessage(PROPS.getProperty("command.searchbooks.invalidisbn"));
                scannedLine = in.nextLine();
            }
        }
        //Выбор автора:
        Integer authorId = null;
        displayManager.displayMessage(PROPS.getProperty("command.searchbooks.chooseauthor"));
        List<Author> authors = authorDao.getAuthors();
        for (int i = 1; i <= authors.size(); i++) {
            displayManager.displayMessage(i + ". " + authors.get(i - 1).NAME);
        }
        scannedLine = in.nextLine();
        while (!scannedLine.equals("")) {
            try {
                int choice = Integer.parseInt(scannedLine);
                if (choice < 1 || choice > authors.size()) throw new IllegalArgumentException();
                authorId = authors.get(choice - 1).ID;
                break;
            } catch (IllegalArgumentException e) {
                displayManager.displayMessage(PROPS.getProperty("command.searchbooks.invalidauthor"));
                scannedLine = in.nextLine();
            }
        }
        //Название книги:
        displayManager.displayMessage(PROPS.getProperty("command.searchbooks.enterbookname"));
        scannedLine = in.nextLine();
        String bookName = scannedLine.equals("") ? null : scannedLine;
        //Дата:
        displayManager.displayMessage(PROPS.getProperty("command.searchbooks.enterdate"));
        scannedLine = in.nextLine();
        //TODO вынести в properties :p
        final String pattern = PROPS.getProperty("command.searchbooks.date.regexp");
        Date[] dates = null;
        while (!scannedLine.equals("")) {
            if (!scannedLine.matches(pattern)) {
                displayManager.displayMessage(PROPS.getProperty("command.searchbooks.invaliddate"));
                scannedLine = in.nextLine();
                continue;
            }
            dates = new Date[2];
            if (scannedLine.contains(PROPS.getProperty("command.searchbooks.date.splitter"))) {
                String[] temp = scannedLine.split(" : ");
                dates[0] = java.sql.Date.valueOf(temp[0]);
                dates[1] = java.sql.Date.valueOf(temp[1]);
            } else {
                dates[0] = java.sql.Date.valueOf(scannedLine);
                dates[1] = new java.sql.Date(dates[0].getTime() + 3600 * 24 * 1000); //next day
            }
            break;
        }

        //Выборка:
        if (isNull(authorId) && isNull(bookName) && isNull(dates)) { //Nothing is specified
            displayManager.displayMessage(PROPS.getProperty("command.searchbooks.nothingisspecified.head"));
            displayManager.displayMessage(PROPS.getProperty("command.searchbooks.nothingisspecified.first"));
            displayManager.displayMessage(PROPS.getProperty("command.searchbooks.nothingisspecified.second"));
            while (true) {
                scannedLine = in.nextLine();
                switch (scannedLine) {
                    case "1":
                        execute();
                        return;
                    case "2":
                        return;
                    default:
                        displayManager.displayMessage(PROPS.getProperty("command.searchbooks.nothingisspecified.invalidchoice"));
                }
            }
            //Это похоже на кирпичную стену
        } else if (isNull(authorId) && isNull(bookName) && !isNull(dates)) { //Specified ONLY DATES
            List<Book> result = bookDao.getBooksByDateInterval(dates[0].getTime(), dates[1].getTime());
            displayBooksAndOfferToDoBookmark(result);
        } else if (isNull(authorId) && !isNull(bookName) && isNull(dates)) { //Specified ONLY BOOKNAME
            List<Book> result = bookDao.getBooksByName(bookName);
            displayBooksAndOfferToDoBookmark(result);
        } else if (isNull(authorId) && !isNull(bookName) && !isNull(dates)) { //Specified BOOKNAME and DATES
            List<Book> result = bookDao.getBooksByNameAndDateInterval(bookName, dates[0].getTime(), dates[1].getTime());
            displayBooksAndOfferToDoBookmark(result);
        } else if (!isNull(authorId) && isNull(bookName) && isNull(dates)) { //Specified ONLY AUTHORID
            List<Book> result = bookDao.getBooksByAuthorId(authorId);
            displayBooksAndOfferToDoBookmark(result);
        } else if (!isNull(authorId) && isNull(bookName) && !isNull(dates)) { //Specified AUTHORID and DATES
            List<Book> result = bookDao.getBooksByAuthorIdAndDateInterval(authorId, dates[0].getTime(), dates[1].getTime());
            displayBooksAndOfferToDoBookmark(result);
        } else if (!isNull(authorId) && !isNull(bookName) && isNull(dates)) { //Specified AUTHORID and BOOKNAME
            List<Book> result = bookDao.getBooksByAuthorIdAndBookName(authorId, bookName);
            displayBooksAndOfferToDoBookmark(result);
        } else if (!isNull(authorId) && !isNull(bookName) && !isNull(dates)) { //Specified AUTHORID and BOOKNAME and DATES
            List<Book> result = bookDao.getBooksByNameAndAuthorIdAndDateInterval(bookName, authorId, dates[0].getTime(), dates[1].getTime());
            displayBooksAndOfferToDoBookmark(result);
        }
    }

    private boolean isNull(Object obj) {
        return obj == null;
    }

    private void displayBooksAndOfferToDoBookmark(List<Book> books) {
        if (books.isEmpty()) {
            DISPLAY_MANAGER.displayMessage(PROPS.getProperty("command.searchbooks.booknotfound"));
            return;
        }
        DISPLAY_MANAGER.displayMessage(PROPS.getProperty("command.searchbooks.bookfoundheader"));
        int i = 1;
        for (Book book : books) {
            String msg = PROPS.getProperty("command.searchbooks.bookpattern")
                    .replaceAll("%num%", String.valueOf(i))
                    .replaceAll("%book%", book.toString());
            DISPLAY_MANAGER.displayMessage(msg);
            i++;
        }
        //Would user like to make bookmarks?
        Scanner in = new Scanner(System.in);
        if (books.size() == 1) {
            DISPLAY_MANAGER.displayMessage(PROPS.getProperty("command.searchbooks.bookmark.addone"));
        } else {
            DISPLAY_MANAGER.displayMessage(PROPS.getProperty("command.searchbooks.bookmark.addtwo"));
        }
        if (!CommandUtils.yesOrNoDialog(in, DISPLAY_MANAGER)) {
            return;
        }
        //Yes, he want to do it, but what book?
        Book targetBook;
        if (books.size() == 1) {
            targetBook = books.get(0);
        } else {
            DISPLAY_MANAGER.displayMessage(PROPS.getProperty("command.searchbooks.bookmark.whatbook")
                    .replaceAll("%booksCount%", String.valueOf(books.size())));
            String scannedLine = in.nextLine();
            while (true) {
                try {
                    int bookNum = Integer.parseInt(scannedLine);
                    if (bookNum < 1 || bookNum > books.size()) {
                        DISPLAY_MANAGER.displayMessage(PROPS.getProperty("command.searchbooks.bookmark.pagetoobigorsmall"));
                        continue;
                    }
                    targetBook = books.get(bookNum - 1);
                    break;
                } catch (NumberFormatException e) {
                    DISPLAY_MANAGER.displayMessage(PROPS.getProperty("command.searchbooks.bookmark.inputnotanumber"));
                }
            }
        }
        //Page?
        DISPLAY_MANAGER.displayMessage(PROPS.getProperty("command.searchbooks.bookmark.whatpage"));
        int page = 0;
        while (true) {
            try {
                page = Integer.parseInt(in.nextLine());
                break;
            } catch (NumberFormatException e) {
                DISPLAY_MANAGER.displayMessage(PROPS.getProperty("command.searchbooks.bookmark.notanumber"));
            }
        }
        //Execution
        if (BOOKMARK_DAO.addBookmark(USER, targetBook, page)) {
            DISPLAY_MANAGER.displayMessage(PROPS.getProperty("command.searchbooks.bookmark.success"));
        } else {
            DISPLAY_MANAGER.displayMessage(PROPS.getProperty("command.searchbooks.bookmark.error"));
        }
    }

}
