package com.epam.jdbc.command;

import com.epam.jdbc.dao.IUserDao;
import com.epam.jdbc.entity.User;
import com.epam.jdbc.mics.IDisplayManager;

import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class ChangeUserRole implements IExecutableCommand {
    private final IDisplayManager DISPLAY_MANAGER;
    private final IUserDao USER_DAO;
    private final User ADMIN_USER;
    private final Properties PROPS;

    public ChangeUserRole(IDisplayManager displayManager, IUserDao userDao, User adminUser, Properties props) {
        DISPLAY_MANAGER = displayManager;
        USER_DAO = userDao;
        this.ADMIN_USER = adminUser;
        PROPS = props;
    }

    @Override
    public void execute() {
        execute(ADMIN_USER, USER_DAO, DISPLAY_MANAGER);
    }

    private void execute(User adminUser, IUserDao userDao, IDisplayManager displayManager) {
        Scanner in = new Scanner(System.in);
        displayManager.displayMessage(PROPS.getProperty("command.changerole.enterusername"));
        String pattern = in.nextLine();
        while ("".equals(pattern)) {
            displayManager.displayMessage(PROPS.getProperty("command.changerole.namemustbenotempty"));
            pattern = in.nextLine();
        }
        List<User> users = userDao.getUsersByNamePattern(pattern);
        User targetUser;
        if (users.size() == 0) {
            displayManager.displayMessage(PROPS.getProperty("command.changerole.nousersfound"));
            if (CommandUtils.yesOrNoDialog(in, displayManager)) {
                execute();
            }
            return;
        } else if (users.size() == 1) {
            targetUser = users.get(0);
            displayManager.displayFormattedMessage(PROPS.getProperty("command.changerole.userfound"), targetUser.NAME, targetUser.ROLE.toString());
        } else {
            displayManager.displayMessage(PROPS.getProperty("command.changerole.someusersfound")
                    .replaceAll("%count%", String.valueOf(users.size())));
            int i = 1;
            for (User user : users) {
                displayManager.displayMessage(i + ". " + user.NAME);
                i++;
            }
            int userNum;
            while (true) {
                try {
                    userNum = Integer.parseInt(in.nextLine());
                    if (userNum > users.size() || userNum < 1) {
                        displayManager.displayMessage(PROPS.getProperty("command.changerole.toobigortoosmall"));
                        continue;
                    }
                    break;
                } catch (NumberFormatException e) {
                    displayManager.displayMessage(PROPS.getProperty("command.changerole.invalidnum"));
                }
            }
            targetUser = users.get(userNum - 1);
            displayManager.displayMessage(PROPS.getProperty("command.changerole.chooserole")
                    .replaceAll("%username%", targetUser.NAME));
        }
        User.Role curr = adminUser.ROLE;
        int i = 1;
        while (curr != null) {
            displayManager.displayMessage(PROPS.getProperty("command.changerole.roleformat")
                    .replaceAll("%num%", String.valueOf(i))
                    .replaceAll("%role%", curr.toString()));
            curr = curr.PARENT;
            i++;
        }
        displayManager.displayMessage(PROPS.getProperty("command.changerole.exititem"));
        int choseRoleNum;
        while (true) {
            String scannedLine = in.nextLine();
            if (scannedLine.equalsIgnoreCase(PROPS.getProperty("command.changerole.exitstr"))) return;
            try {
                choseRoleNum = Integer.parseInt(scannedLine);
                if (choseRoleNum >= i || choseRoleNum < 1) {
                    displayManager.displayMessage(PROPS.getProperty("command.changerole.toobigortoosmall"));
                    continue;
                }
                break;
            } catch (NumberFormatException e) {
                displayManager.displayMessage(PROPS.getProperty("command.changerole.invalidnum"));
            }
        }
        curr = adminUser.ROLE;
        while (choseRoleNum-- > 1) {
            curr = curr.PARENT;
        }
        if(userDao.changeUserRole(targetUser, curr)) {
            displayManager.displayMessage(PROPS.getProperty("command.changerole.success"));
        } else {
            displayManager.displayMessage(PROPS.getProperty("command.changerole.error"));
        }
    }

    @Override
    public String getDescription() {
        return PROPS.getProperty("command.changerole.description");
    }

    @Override
    public User.Role getAllowedForRole() {
        return User.Role.ADMIN;
    }
}
