package com.epam.jdbc.dao;

import com.epam.jdbc.entity.Author;

import java.util.List;

public interface IAuthorDao extends IBaseDao {
    List<Author> getAuthors();

    Author getAuthorById(int id);

    List<Author> getAuthorsByName(String namePattern);
}
