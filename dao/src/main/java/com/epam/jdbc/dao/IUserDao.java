package com.epam.jdbc.dao;

import com.epam.jdbc.entity.User;

import java.util.List;

public interface IUserDao extends IBaseDao {
    User getUserByNameAndPassword(String username, String password);

    User getUserById(int id);

    boolean addUserToDatabase(User user);

    List<User> getUsersByNamePattern(String namePattern);

    boolean changeUserRole(User user, User.Role toRole);
}
