package com.epam.jdbc.dao.impl;

import com.epam.jdbc.connectivity.IDatabaseManager;
import com.epam.jdbc.dao.IBookDao;
import com.epam.jdbc.entity.Book;
import com.epam.jdbc.mics.IDisplayManager;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BookDao implements IBookDao {
    private final IDatabaseManager DBMANAGER;
    private final IDisplayManager DISPLAY_MANAGER;

    public BookDao(IDatabaseManager dbManager, IDisplayManager displayManager) {
        this.DBMANAGER = dbManager;
        this.DISPLAY_MANAGER = displayManager;
    }

    @Override
    public Book getBookByISBN(long isbn) {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "SELECT `isbn`, `bookName`, `authorName`, `date` FROM `books` JOIN `authors` USING (`authorId`) WHERE `isbn` = ? AND `isActive` = 1";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setLong(1, isbn);
                try (ResultSet resultSet = stmt.executeQuery()) {
                    if (!resultSet.next()) return null;
                    return extractBookFromResultSet(resultSet);
                }
            }
        } catch (SQLException e) {
            DISPLAY_MANAGER.displayError(e);
        }
        return null;
    }

    @Override
    public List<Book> getBooksByAuthorId(int authorId) {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "SELECT `isbn`, `bookName`, `authorName`, `date` FROM `books` JOIN `authors` USING (`authorId`)  WHERE `authorId` = ?  AND `isActive` = 1";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setInt(1, authorId);
                try (ResultSet resultSet = stmt.executeQuery()) {
                    List<Book> result = new ArrayList<>();
                    while (resultSet.next()) {
                        result.add(extractBookFromResultSet(resultSet));
                    }
                    return result;
                }
            }
        } catch (SQLException e) {
            DISPLAY_MANAGER.displayError(e);
        }
        return null;
    }

    @Override
    public List<Book> getBooksByName(String bookNamePattern) {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "SELECT `isbn`, `bookName`, `authorName`, `date` FROM `books` JOIN `authors` USING (`authorId`)  WHERE `bookName` LIKE ?  AND `isActive` = 1";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setString(1, "%" + bookNamePattern + "%");
                try (ResultSet resultSet = stmt.executeQuery()) {
                    List<Book> result = new ArrayList<>();
                    while (resultSet.next()) {
                        result.add(extractBookFromResultSet(resultSet));
                    }
                    return result;
                }
            }
        } catch (SQLException e) {
            DISPLAY_MANAGER.displayError(e);
        }
        return null;
    }

    @Override
    public List<Book> getBooksByDateInterval(long firstTimestamp, long secondTimestamp) {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "SELECT `isbn`, `bookName`, `authorName`, `date` FROM `books` JOIN `authors` USING (`authorId`)  WHERE `date` BETWEEN FROM_UNIXTIME(?) AND FROM_UNIXTIME(?)  AND `isActive` = 1";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setLong(1, firstTimestamp / 1000);
                stmt.setLong(2, secondTimestamp / 1000);
                try (ResultSet resultSet = stmt.executeQuery()) {
                    List<Book> result = new ArrayList<>();
                    while (resultSet.next()) {
                        result.add(extractBookFromResultSet(resultSet));
                    }
                    return result;
                }
            }
        } catch (SQLException e) {
            DISPLAY_MANAGER.displayError(e);
        }
        return null;
    }


    @Override
    public boolean addBookToDatabase(Book book) {
        try (Connection connection = DBMANAGER.getConnection()) {
            int authorId = getAuthorId(connection, book.AUTHOR);
            //Adding new author if he doesn't exist:
            if (authorId == -1) {
                addAuthor(connection, book.AUTHOR);
                authorId = getAuthorId(connection, book.AUTHOR);
                if (authorId == -1) throw new IllegalStateException("Something went wrong while adding new author!");
            }
            //Adding new book:
            String query = "INSERT INTO `books` (`isbn`, `bookName`, `authorId`, `date`) VALUES (?, ?, ?, ?)";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setLong(1, book.ISBN);
                stmt.setString(2, book.NAME);
                stmt.setInt(3, authorId);
                stmt.setDate(4, book.DATE);
                stmt.execute();
                return true;
            }
        } catch (SQLException e) {
            DISPLAY_MANAGER.displayError(e);
        }
        return false;
    }

    @Override
    public List<Book> getBooksByAuthorIdAndBookName(int authorId, String bookName) {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "SELECT `isbn`, `bookName`, `authorName`, `date` FROM `books` JOIN `authors` USING (`authorId`) WHERE `bookName` LIKE ? AND authorId = ?  AND `isActive` = 1";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setString(1, "%" + bookName + "%");
                stmt.setLong(2, authorId);
                try (ResultSet resultSet = stmt.executeQuery()) {
                    List<Book> result = new ArrayList<>();
                    while (resultSet.next()) {
                        result.add(extractBookFromResultSet(resultSet));
                    }
                    return result;
                }
            }
        } catch (SQLException e) {
            DISPLAY_MANAGER.displayError(e);
        }
        return null;
    }

    @Override
    public List<Book> getBooksByAuthorIdAndDateInterval(int authorId, long firstTimestamp, long secondTimestamp) {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "SELECT `isbn`, `bookName`, `authorName`, `date` FROM `books`JOIN `authors` USING (`authorId`) WHERE `date` BETWEEN FROM_UNIXTIME(?) AND FROM_UNIXTIME(?) AND authorId = ?  AND `isActive` = 1";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setLong(1, firstTimestamp);
                stmt.setLong(2, secondTimestamp);
                stmt.setInt(3, authorId);
                try (ResultSet resultSet = stmt.executeQuery()) {
                    List<Book> result = new ArrayList<>();
                    while (resultSet.next()) {
                        result.add(extractBookFromResultSet(resultSet));
                    }
                    return result;
                }
            }
        } catch (SQLException e) {
            DISPLAY_MANAGER.displayError(e);
        }
        return null;
    }

    @Override
    public List<Book> getBooksByNameAndDateInterval(String bookNamePattern, long firstTimestamp, long secondTimestamp) {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "SELECT `isbn`, `bookName`, `authorName`, `date` FROM `books` JOIN `authors` USING (`authorId`) WHERE `date` BETWEEN FROM_UNIXTIME(?) AND FROM_UNIXTIME(?) AND `bookName` LIKE ?  AND `isActive` = 1";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setLong(1, firstTimestamp);
                stmt.setLong(2, secondTimestamp);
                stmt.setString(3, "%" + bookNamePattern + "%");
                try (ResultSet resultSet = stmt.executeQuery()) {
                    List<Book> result = new ArrayList<>();
                    while (resultSet.next()) {
                        result.add(extractBookFromResultSet(resultSet));
                    }
                    return result;
                }
            }
        } catch (SQLException e) {
            DISPLAY_MANAGER.displayError(e);
        }
        return null;
    }

    @Override
    public List<Book> getBooksByNameAndAuthorIdAndDateInterval(String bookName, int authorId, long firstTimestamp, long secondTimestamp) {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "SELECT `isbn`, `bookName`, `authorName`, `date` FROM `books`JOIN `authors` USING (`authorId`) WHERE `date` BETWEEN FROM_UNIXTIME(?) AND FROM_UNIXTIME(?) AND authorId = ? AND `bookName` LIKE ?  AND `isActive` = 1";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setLong(1, firstTimestamp);
                stmt.setLong(2, secondTimestamp);
                stmt.setInt(3, authorId);
                stmt.setString(4, "%" + bookName + "%");
                try (ResultSet resultSet = stmt.executeQuery()) {
                    List<Book> result = new ArrayList<>();
                    while (resultSet.next()) {
                        result.add(extractBookFromResultSet(resultSet));
                    }
                    return result;
                }
            }
        } catch (SQLException e) {
            DISPLAY_MANAGER.displayError(e);
        }
        return null;
    }

    @Override
    public boolean deleteBookByIsbn(long isbn) {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "UPDATE `books` SET `isActive` = 0 WHERE `ISBN` = ?";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setLong(1, isbn);
                stmt.executeUpdate();
                return true;
            }
        } catch (SQLException e) {
            DISPLAY_MANAGER.displayError(e);
            return false;
        }
    }

    private Book extractBookFromResultSet(ResultSet resultSet) throws SQLException {
        long isbn = resultSet.getLong("isbn");
        String bookName = resultSet.getString("bookName");
        String authorName = resultSet.getString("authorName");
        Date date = resultSet.getDate("date");
        return new Book(isbn, bookName, authorName, date);
    }

    private int getAuthorId(Connection connection, String authorName) throws SQLException {
        String query = "SELECT `authorId` FROM `authors` WHERE `authorName` = ?";
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setString(1, authorName);
            try (ResultSet resultSet = stmt.executeQuery()) {
                if (resultSet.next()) return resultSet.getInt("authorId");
            }
        }
        return -1;
    }

    private void addAuthor(Connection connection, String authorName) throws SQLException {
        String query = "INSERT INTO `authors` (`authorName`) VALUES (?)";
        try (PreparedStatement stmt = connection.prepareStatement(query)) {
            stmt.setString(1, authorName);
            stmt.execute();
        }
    }
}
