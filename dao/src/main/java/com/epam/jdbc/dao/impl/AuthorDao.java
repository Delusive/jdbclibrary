package com.epam.jdbc.dao.impl;

import com.epam.jdbc.connectivity.IDatabaseManager;
import com.epam.jdbc.dao.IAuthorDao;
import com.epam.jdbc.entity.Author;
import com.epam.jdbc.mics.IDisplayManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AuthorDao implements IAuthorDao {
    private final IDatabaseManager DBMANAGER;
    private final IDisplayManager DISPLAY_MANAGER;

    public AuthorDao(IDatabaseManager dbManager, IDisplayManager displayManager) {
        this.DBMANAGER = dbManager;
        this.DISPLAY_MANAGER = displayManager;
    }

    @Override
    public List<Author> getAuthors() {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "SELECT `authorId`, `authorName` FROM `authors`";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                try (ResultSet resultSet = stmt.executeQuery()) {
                    List<Author> result = new ArrayList<>();
                    while (resultSet.next()) {
                        int authorId = resultSet.getInt("authorId");
                        String authorName = resultSet.getString("authorName");
                        result.add(new Author(authorId, authorName));
                    }
                    return result;
                }
            }
        } catch (SQLException e) {
            DISPLAY_MANAGER.displayError(e);
        }
        return null;
    }

    @Override
    public Author getAuthorById(int id) {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "SELECT `authorId`, `authorName` FROM `authors` WHERE `authorId` = ?";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setInt(1, id);
                try (ResultSet resultSet = stmt.executeQuery()) {
                    if (!resultSet.next()) return null;
                    int authorId = resultSet.getInt("authorId");
                    String authorName = resultSet.getString("authorName");
                    return new Author(authorId, authorName);
                }
            }
        } catch (SQLException e) {
            DISPLAY_MANAGER.displayError(e);
        }
        return null;
    }

    @Override
    public List<Author> getAuthorsByName(String namePattern) {
        try (Connection connection = DBMANAGER.getConnection()) {
            String query = "SELECT `authorId`, `authorName` FROM `authors` WHERE `authorName` LIKE ?";
            try (PreparedStatement stmt = connection.prepareStatement(query)) {
                stmt.setString(1, "%" + namePattern + "%");
                try (ResultSet resultSet = stmt.executeQuery()) {
                    List<Author> result = new ArrayList<>();
                    while (resultSet.next()) {
                        int authorId = resultSet.getInt("authorId");
                        String authorName = resultSet.getString("authorName");
                        result.add(new Author(authorId, authorName));
                    }
                    return result;
                }
            }
        } catch (SQLException e) {
            DISPLAY_MANAGER.displayError(e);
        }
        return null;
    }
}
