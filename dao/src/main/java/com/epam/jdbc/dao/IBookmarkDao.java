package com.epam.jdbc.dao;

import com.epam.jdbc.entity.Book;
import com.epam.jdbc.entity.Bookmark;
import com.epam.jdbc.entity.User;

import java.util.List;

public interface IBookmarkDao extends IBaseDao {
    List<Bookmark> getUserBookmarks(User user);

    boolean deleteBookmarkById(int id);

    boolean addBookmark(User user, Book book, int page);
}
