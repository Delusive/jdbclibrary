package com.epam.jdbc.entity;

import java.sql.Date;

public class Book extends Entity {
    public final long ISBN;
    public final String NAME;
    public final String AUTHOR;
    public final Date DATE;

    public Book(long isbn, String name, String author, Date date) {
        this.ISBN = isbn;
        this.NAME = name;
        this.AUTHOR = author;
        this.DATE = date;
    }

    @Override
    public String toString() {
        return String.format("ISBN: %s | Name: %s | Author: %s | Publish Date: %s",
                ISBN,
                NAME,
                AUTHOR,
                DATE);
    }
}
