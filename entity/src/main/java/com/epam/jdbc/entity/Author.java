package com.epam.jdbc.entity;

public class Author extends Entity {
    public final int ID;
    public final String NAME;

    public Author(int id, String name) {
        this.ID = id;
        this.NAME = name;
    }
}
