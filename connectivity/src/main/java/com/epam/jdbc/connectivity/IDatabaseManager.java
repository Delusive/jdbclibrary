package com.epam.jdbc.connectivity;

import java.sql.Connection;
import java.sql.SQLException;

public interface IDatabaseManager {
    Connection getConnection() throws SQLException;
}
