package com.epam.jdbc.connectivity;

import com.mysql.cj.jdbc.MysqlDataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class MySQLDatabaseManager implements IDatabaseManager {
    private final MysqlDataSource DATASOURCE = new MysqlDataSource();

    public MySQLDatabaseManager(Properties props) {
        DATASOURCE.setUrl(props.getProperty("db.url"));
        DATASOURCE.setUser(props.getProperty("db.username"));
        DATASOURCE.setPassword(props.getProperty("db.password"));
    }

    @Override
    public Connection getConnection() throws SQLException {
        return DATASOURCE.getConnection();
    }
}
