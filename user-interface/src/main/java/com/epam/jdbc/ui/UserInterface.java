package com.epam.jdbc.ui;

import com.epam.jdbc.command.*;
import com.epam.jdbc.connectivity.IDatabaseManager;
import com.epam.jdbc.connectivity.MySQLDatabaseManager;
import com.epam.jdbc.dao.IAuthorDao;
import com.epam.jdbc.dao.IBookDao;
import com.epam.jdbc.dao.IBookmarkDao;
import com.epam.jdbc.dao.IUserDao;
import com.epam.jdbc.dao.impl.AuthorDao;
import com.epam.jdbc.dao.impl.BookDao;
import com.epam.jdbc.dao.impl.BookmarkDao;
import com.epam.jdbc.dao.impl.UserDao;
import com.epam.jdbc.entity.User;
import com.epam.jdbc.mics.ConsoleDisplayManager;
import com.epam.jdbc.mics.IDisplayManager;
import com.epam.jdbc.mics.PropertiesManager;

import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class UserInterface implements IUserInterface {
    private IDisplayManager displayManager = new ConsoleDisplayManager();
    private PropertiesManager settingsPropertiesManager = new PropertiesManager("/library.properties", displayManager);
    private PropertiesManager localizationPropertiesManager = new PropertiesManager("/messages.properties", displayManager);
    private IDatabaseManager databaseManager = new MySQLDatabaseManager(settingsPropertiesManager.getProperties("db.url", "db.username", "db.password"));
    private IUserDao userDao = new UserDao(databaseManager, displayManager);
    private IBookDao bookDao = new BookDao(databaseManager, displayManager);
    private IAuthorDao authorDao = new AuthorDao(databaseManager, displayManager);
    private IBookmarkDao bookmarkDao = new BookmarkDao(databaseManager, displayManager);
    private User user;

    @Override
    public void delegate() {
        Scanner scanner = new Scanner(System.in);
        authorization(scanner);
        registerCommands();
        while (true) {
            displayManager.displayMessage(localizationPropertiesManager.getString("menu.header.top"));
            displayManager.displayMessage(localizationPropertiesManager.getString("menu.header.middle"));
            displayManager.displayMessage(localizationPropertiesManager.getString("menu.header.bot"));
            showMenu();
            displayManager.displayMessage(localizationPropertiesManager.getString("menu.footer.bot"));
            while (true) {
                int choseActionNumber;
                try {
                    choseActionNumber = Integer.parseInt(scanner.nextLine());
                } catch (NumberFormatException e) {
                    displayManager.displayMessage(localizationPropertiesManager.getString("menu.notanumber"));
                    continue;
                }
                //if he want to logout:
                if(choseActionNumber == 0) {
                    user = null;
                    authorization(scanner);
                    registerCommands();
                }
                processCommand(choseActionNumber);
                displayManager.displayMessage(localizationPropertiesManager.getString("menu.pressenter"));
                scanner.nextLine();
                break;
            }
        }
    }

    private void registerCommands() {
        Properties messages = localizationPropertiesManager.getProperties();
        CommandManager.registerCommand(new SearchBooksAndMakingBookmarks(bookDao, authorDao, bookmarkDao, displayManager, user, messages));
        CommandManager.registerCommand(new AddBookToDB(bookDao, displayManager, messages));
        CommandManager.registerCommand(new ShowAndDeleteBookmarks(displayManager, bookmarkDao, user, messages));
        CommandManager.registerCommand(new DeleteBookFromDB(displayManager, bookDao, messages));
        CommandManager.registerCommand(new AddNewUser(displayManager, userDao, messages));
        CommandManager.registerCommand(new ChangeUserRole(displayManager, userDao, user, messages));
    }

    private void processCommand(int actionNumber) {
        List<IExecutableCommand> commands = CommandManager.getAllowedCommandsFor(user.ROLE);
        if (actionNumber > commands.size() || actionNumber < 1) {
            displayManager.displayMessage(localizationPropertiesManager.getString("menu.invalidchoice"));
            return;
        }
        commands.get(actionNumber - 1).execute();
    }

    private void showMenu() {
        StringBuilder sb = new StringBuilder("* 0. Logout");
        while (sb.length() != localizationPropertiesManager.getString("menu.header.bot").length() - 1) sb.append(' ');
        sb.append('*');
        displayManager.displayMessage(sb.toString());

        int i = 1;
        for (IExecutableCommand c : CommandManager.getAllowedCommandsFor(user.ROLE)) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("* ").append(i).append(". ").append(c.getDescription());
            while (stringBuilder.length() != localizationPropertiesManager.getString("menu.header.bot").length() - 1) stringBuilder.append(' ');
            stringBuilder.append('*');
            displayManager.displayMessage(stringBuilder.toString());
            i++;
        }
    }

    private void authorization(Scanner scanner) {
        displayManager.displayMessage(localizationPropertiesManager.getString("menu.authrequest.username"));
        String username = scanner.nextLine();
        displayManager.displayMessage(localizationPropertiesManager.getString("menu.authrequest.password"));
        while (true) {
            String password = scanner.nextLine();
            if(password.equalsIgnoreCase(":rename")) {
                authorization(scanner);
                return;
            }
            User user = userDao.getUserByNameAndPassword(username, password);
            if (user != null) {
                this.user = user;
                displayManager.displayFormattedMessage(localizationPropertiesManager.getString("menu.authrequest.success")
                        .replaceAll("%username%", user.NAME)
                        .replaceAll("%role%", user.ROLE.toString()));
                break;
            }
            displayManager.displayMessage(localizationPropertiesManager.getString("menu.authrequest.error"));
        }

    }
}
