package com.epam.jdbc;

import com.epam.jdbc.ui.IUserInterface;
import com.epam.jdbc.ui.UserInterface;

public class Main {
    private static IUserInterface userInterface = new UserInterface();

    public static void main(String[] args) {
        userInterface.delegate();
    }
}
